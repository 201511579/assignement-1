#lang racket
(define (age yob)
  (- 2015 yob)
 )
(define (wizardingTest yob)
(cond
  [(string? yob)
   "confusus charm"]
  [(=  (age yob) 16)
    "OWLs"]
    [(= (age yob) 18)
     "NEWTs"]
    [(< (age yob) 16)
     "Whats the hurry"]
    [(> (age yob) 18)
     "Too old for school!"]
    [else "Confundus charm?"]
   )
  )
  